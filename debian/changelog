libfile-share-perl (0.27-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libfile-share-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 07 Dec 2022 22:44:41 +0000

libfile-share-perl (0.27-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.27.
  * New test dependency: libreadonly-perl.
  * Skip new t/readonly-dollar-underscore.t during autopkgtests.

 -- gregor herrmann <gregoa@debian.org>  Mon, 01 Aug 2022 20:18:38 +0200

libfile-share-perl (0.26-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.26.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Use HTTPS in debian/watch and debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 28 Jul 2022 16:41:06 +0200

libfile-share-perl (0.25-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Fix day-of-week for changelog entry 0.02-1.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 22:34:19 +0100

libfile-share-perl (0.25-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 18:52:42 +0100

libfile-share-perl (0.25-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * New upstream release 0.17.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Imported upstream version 0.25
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Wed, 08 Oct 2014 23:18:33 +0200

libfile-share-perl (0.16-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sun, 27 Jul 2014 15:58:16 +0200

libfile-share-perl (0.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Install new CONTRIBUTING file.

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Jul 2014 21:59:23 +0200

libfile-share-perl (0.14-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 0.14

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 29 Jun 2014 05:40:13 +0200

libfile-share-perl (0.13-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 Jun 2014 17:06:46 +0200

libfile-share-perl (0.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Jun 2014 14:11:34 +0200

libfile-share-perl (0.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * debian/rules: drop override. The manpage is created correctly now.
  * Update debian/copyright.
    Bump copyright years, drop info about removed files.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 May 2014 14:39:44 +0200

libfile-share-perl (0.03-1) unstable; urgency=low

  * Team upload.

  [ Jonas Smedegaard ]
  * Fix module name and improve spelling in long description.
  * Fix shorten short description to stay below 80 chars.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright.
  * Use canonical URL in Vcs-Git.
  * Re-create manpage from .pod instead of .pm file. Fix for manpage-has-
    bad-whatis-entry detected by lintian.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Oct 2013 22:58:49 +0200

libfile-share-perl (0.02-1) unstable; urgency=low

  * Initial Release. (Closes: #704060)

 -- Joenio Costa <joenio@colivre.coop.br>  Tue, 26 Mar 2013 22:23:24 -0300
